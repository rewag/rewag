#!/usr/bin/python
# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.colors import *
import sys


@runs_once
def clear():
    with settings(warn_only=True):
        with lcd(sys.path[0]+'/../../../../'):
            local('rm -f composer.lock')
            local('composer remove friendsofsymfony/user-bundle --no-update')
            local('composer remove snc/redis-bundle --no-update')
            local('composer update')

clear()

