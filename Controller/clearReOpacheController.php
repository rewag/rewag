<?php
/**
 * Created by PhpStorm.
 * User: kcswag
 * Date: 6/27/17
 * Time: 3:34 PM
 */

namespace rewag\rewagBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;



class clearReOpacheController extends Controller
{
    /**
     * @Route("/rewag")
     */
    public function clearRe(){
        return $this->render("rewagBundle:Default:rewag.html.twig");
    }


    /**
     * @Route("/rewag/check_code",name="rewag_check_code")
     */
    public function checkCode(Request $request){
        if($request->get('check_code')=='rewag12345678'){

            exec("fab -f ".__DIR__."/re_clear.py --list",$op,$status);

            print_r(['op'=>$op,'status'=>$status]);
            exit;

        }else{
            return new Response("Invalid check code!");
        }
    }

}